<?php 
//Starting the session
session_start();
//This is the default language. We will use it 2 places, so i am put it 
//into a varaible.
$defaultLang = 'ru';

//Checking, if the $_GET["language"] has any value
//if the $_GET["language"] is not empty
if (!empty($_GET["language"])) { //<!-- see this line. checks 
    //Based on the lowecase $_GET['language'] value, we will decide,
    //what lanuage do we use
    switch (strtolower($_GET["language"])) {
        case "ru":
            //If the string is en or EN
            $_SESSION['lang'] = 'ru';
            break;
        case "en":
            //If the string is tr or TR
            $_SESSION['lang'] = 'en';
            break;
        case "az":
            //If the string is tr or TR
            $_SESSION['lang'] = 'az';
            break;
        default:
            //IN ALL OTHER CASES your default langauge code will set
            //Invalid languages
            $_SESSION['lang'] = $defaultLang;
            break;
    }
}

$_SERVER['DOCUMENT_ROOT']= 'C:/xampp/htdocs/rubicon/';
?>