<?php
	class Database {
		// DB PARAMS
		private $user = 'root';
		private $pass = '';
		private $dbName = 'rubicon';
		private $dbHost = 'localhost';
		private $conn;

		// DB CONNECT
		public function connect(){
			$this->conn = null;

			try {
			    $this->conn = new PDO("mysql:host=$this->dbHost;charset=utf8;dbname=$this->dbName;",$this->user,$this->pass);
			    // set the PDO error mode to exception
			    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $e){
			    echo "Connection failed: " . $e->getMessage();
			    echo 'Exception -> ';
			    var_dump($e->getMessage());
			}
		return $this->conn;
		}
	}
?>