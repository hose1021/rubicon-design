<?php
class Text{
	public $ID;
	public $RU;
	public $EN;
	public $AZ;
	private $conn;
	private $table = 'langueage';

	public function __construct($db){
		$this->conn = $db;
	}
	// Read from db lang text
	public function read(){
		$queary = 'SELECT
			ID,
			RU,
			EN,
			AZ
			FROM '
			. $this->table
		;
		$stmn = $this->conn->prepare($queary);
		$stmn->execute();

		return $stmn;
	}
	// Pushing text in DB
	public function push(){
		$queary = 'INSERT INTO' .$this->table.'
			SET 
				RU = :RU,
				EN = :EN,
				AZ = :AZ
		';
		$stmn = $this->conn->prepare($queary);
		
		//Clean data
		$this->RU = htmlspecialchars(strip_tags($this->RU));

		//Bind data
		$stmn->bindParam(':RU', $this->RU);
	}
}
?>