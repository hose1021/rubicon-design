<?php 
require_once './config/config.php';
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Rubicon</title>
  <link href='https://fonts.googleapis.com/css?family=Raleway:500,400,300,200,100' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link rel="stylesheet" href="css/twentytwenty.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/nprogress.css">

</head>
<body>
  <?php include 'left-side.php';?>
  <div class="right">
    <section class="head">
      <div class="code">
        <h1>Мы превращаем<div><span class="works">код</span> в <span class="works">шедевр</span></div></h1>
      </div>
      <div id="container1">
        <img src="http://hackflow.com/images/reform/reform-navigate.gif">
        <img src="http://its-me.us/data/upload/2016/05/easy-website-creator-01.jpg">
      </div>
    </section>

    <section class="blocks">
        <div class="serviceBox">
          <div class="service-icon">
              <span><i class="fas fa-object-group"></i></span>
          </div>
          <div class="service-content">
              <h3 class="title">Веб дизайн</h3>
              <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
          </div>
        </div>
          <div class="serviceBox green">
              <div class="service-icon">
                  <span><i class="fa fa-desktop"></i></span>
              </div>
              <div class="service-content">
                  <h3 class="title">Веб разработка</h3>
                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
              </div>
          </div>

          <div class="serviceBox orange">
              <div class="service-icon">
                  <span><i class="fa fa-tablet"></i></span>
              </div>
              <div class="service-content">
                  <h3 class="title">Логотипы</h3>
                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
              </div>
          </div>
        <div class="serviceBox blue">
          <div class="service-icon">
              <span><i class="fas fa-home"></i></span>
          </div>
          <div class="service-content">
            <h3 class="title">3D моделирование</h3>
            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
          </div>
        </div>
        <div class="serviceBox blue">
          <div class="service-icon">
              <span><i class="fa fa-shopping-cart"></i></span>
          </div>
          <div class="service-content">
            <h3 class="title">E-Commarce</h3>
            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
          </div>
        </div>
        <div class="serviceBox blue">
          <div class="service-icon">
              <span><i class="fa fa-shopping-cart"></i></span>
          </div>
          <div class="service-content">
            <h3 class="title">E-Commarce</h3>
            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
          </div>
        </div>
        <div class="serviceBox blue">
          <div class="service-icon">
              <span><i class="fa fa-shopping-cart"></i></span>
          </div>
          <div class="service-content">
            <h3 class="title">E-Commarce</h3>
            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
          </div>
        </div>
        <div class="serviceBox blue">
          <div class="service-icon">
              <span><i class="fa fa-shopping-cart"></i></span>
          </div>
          <div class="service-content">
            <h3 class="title">E-Commarce</h3>
            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
          </div>
        </div>
    </section>
  </div>
</body>
<script async src="https://www.youtube.com/iframe_api"></script>
<script src="js/youtube.js"></script>
<script src="js/quote.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/jquery.event.move.js" type="text/javascript"></script>
<script src="js/jquery.twentytwenty.js" type="text/javascript"></script>
<script src="js/nprogress.js"></script>
<script src="js/load.js"></script>
<script>
  $(function(){
  $("#container1").twentytwenty();
  });
</script>
</html>