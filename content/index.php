<?php
require_once '../config/config.php';
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Rubicon</title>
        <link href='https://fonts.googleapis.com/css?family=Raleway:500,400,300,200,100' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/infinite-slider.css">
        <link rel="stylesheet" href="../css/nprogress.css">
    </head>

    <body>
        <?php require_once 'left-side.php';?>
        <div class="right">
            <section>
                <div class="background"></div>
                <div class="head">
                    <canvas class="container" id="container"></canvas>
                    <div class="blur blurTop">
                        <canvas class="canvas" id="blurCanvasTop"></canvas>
                    </div>
                    <div class="blur blurBottom">
                        <canvas width="100px" height="1000px" class="canvas" id="blurCanvasBottom"></canvas>
                    </div>
                    <div class="title">
                        <div class="title-text">
                            <h1><?php if ($_SESSION['lang'] == "ru") { echo $RU_arr[6];} elseif ($_SESSION['lang'] == "en") { echo $EN_arr[6];} elseif ($_SESSION['lang'] == "az") { echo $AZ_arr[6];}?></h1>
                            <div class="text">
                                <?php if ($_SESSION['lang'] == "ru") { echo $RU_arr[7];} elseif ($_SESSION['lang'] == "en") { echo $EN_arr[7];} elseif ($_SESSION['lang'] == "az") { echo $AZ_arr[7];}?><span class="works"><?php if ($_SESSION['lang'] == "ru") { echo "<span id='appru'></span>";} elseif ($_SESSION['lang'] == "en") { echo "<span id='appen'></span>";} elseif ($_SESSION['lang'] == "az") { echo "<span id='appaz'></span>";}?></span>
                            </div>
                            <a href="services.php">
                                <button class="btn">
                                    <?php if ($_SESSION['lang'] == "ru") { echo $RU_arr[8];} elseif ($_SESSION['lang'] == "en") { echo $EN_arr[8];} elseif ($_SESSION['lang'] == "az") { echo $AZ_arr[8];}?>
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="image">
                        <img src="images/headerman.png" width="100%">
                    </div>
                    <div class="arrow bounce"></div>
                </div>
            </section>
            <section class="services">
                <div class="title">
                    <h1>Наши услуги</h1>
                </div>
                <div class="blocks-index">
                    <div class="serviceBox">
                        <div class="service-icon">
                            <span><i class="fas fa-object-group"></i></span>
                        </div>
                        <div class="service-content">
                            <h3 class="title">Веб дизайн</h3>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
                        </div>
                    </div>
                    <div class="serviceBox green">
                        <div class="service-icon">
                            <span><i class="fa fa-desktop"></i></span>
                        </div>
                        <div class="service-content">
                            <h3 class="title">Веб разработка</h3>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
                        </div>
                    </div>
                    <div class="serviceBox orange">
                        <div class="service-icon">
                            <span><i class="fab fa-contao"></i></span>
                        </div>
                        <div class="service-content">
                            <h3 class="title">Логотипы</h3>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
                        </div>
                    </div>
                    <div class="serviceBox blue">
                        <div class="service-icon">
                            <span><i class="fas fa-home"></i></span>
                        </div>
                        <div class="service-content">
                            <h3 class="title">3D моделирование</h3>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in volutpat elit. Class aptent taciti.</p>
                        </div>
                    </div>
                </div>
                <a href="services.php">
                    <button class="btn">Подробнее</button>
                </a>
            </section>
            <section class="portfolio">
                <div class="title">
                    <h1>Наши работы</h1>
                </div>
                <div id="portfolio">
                    <ul class="portfolio-grid">
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/1abc9c/FFFFFF" alt="img01"/>
						</a>
                        </li>
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/2ecc71/FFFFFF" alt="img01"/>
						</a>
                        </li>
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/3498db/FFFFFF" alt="img01"/>
						</a>
                        </li>
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/9b59b6/FFFFFF" alt="img01"/>
						</a>
                        </li>
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/16a085/FFFFFF" alt="img01"/>
						</a>
                        </li>
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/27ae60/FFFFFF" alt="img01"/>
						</a>
                        </li>
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/2980b9/FFFFFF" alt="img01"/>
						</a>
                        </li>
                        <li>
                            <a href="#" class="animated flipInX">
							<img src="http://placehold.it/200x200/8e44ad/FFFFFF" alt="img01"/>
						</a>
                        </li>
                    </ul>
                </div>
            </section>
            <section class="brands">
                <div class="title">
                    <h1>Наши клиенты</h1>
                </div>
                <section class="customer-logos slider">
                    <div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"></div>
                    <div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"></div>
                    <div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"></div>
                    <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
                    <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
                    <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
                    <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
                    <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>
                </section>
            </section>
            <section class="contact">
            	
            </section>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script async src="https://www.youtube.com/iframe_api"></script>
    <script src="../js/youtube.js"></script>
    <script src="../js/quote.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="../js/head.js"></script>
    <script src="../js/typewriter.js"></script>
    <script src="../js/nprogress.js"></script>
    <script src="../js/load.js"></script>
    <script>
    if (document.getElementById('appru') !== null) {
        var appru = document.getElementById('appru');

        var typewriter = new Typewriter(appru, {
            loop: true
        });

        typewriter.typeString(' создавать сайты')
            .pauseFor(2000)
            .deleteAll()
            .typeString(' создавать 3D модели')
            .pauseFor(2000)
            .deleteAll()
            .typeString(' радовать :)')
            .pauseFor(2000)
            .deleteAll()
            .start();
    } else if (document.getElementById('appen') !== null) {
        var appen = document.getElementById('appen');

        var typewriter = new Typewriter(appen, {
            loop: true
        });

        typewriter.typeString(' create sites')
            .pauseFor(2000)
            .deleteAll()
            .typeString(' create 3D models')
            .pauseFor(2000)
            .deleteAll()
            .typeString(' rejoice :)')
            .pauseFor(2000)
            .deleteAll()
            .start();
    } else if (document.getElementById('appaz') !== null) {
        var appen = document.getElementById('appaz');

        var typewriter = new Typewriter(appaz, {
            loop: true
        });

        typewriter.typeString(' veb saytlar yaradırıq')
            .pauseFor(2000)
            .deleteAll()
            .typeString(' 3D modelləri yaradırıq')
            .pauseFor(2000)
            .deleteAll()
            .typeString(' insanları xoşbəxt edirik :)')
            .pauseFor(2000)
            .deleteAll()
            .start();
    }
    </script>
    <script>
    var $jq = jQuery.noConflict();
    $jq(document).ready(function() {
        $('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    });
    </script>
    </html>