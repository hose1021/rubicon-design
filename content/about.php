<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/rubicon/config/config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Rubicon</title>
    <link href='https://fonts.googleapis.com/css?family=Raleway:500,400,300,200,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/twentytwenty.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/foundation.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/nprogress.css">
</head>

<body>
    <?php include 'left-side.php';?>
    <div class="right">
        <section class="head">
            <div class="about-header-bg"></div>
            <div id="work-place" class="work-place"></div>
            <div class="aboutus">
                <h1>О нас</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore cupiditate, autem adipisci temporibus libero, laboriosam eos accusamus vitae at ipsum ducimus ut dolorum ipsam nisi dolore consectetur mollitia molestias totam!</p>
                <a href="services.php">
	                <button class="btn">
	                    <?php if ($_SESSION['lang'] == "ru") { echo $RU_arr[8];} elseif ($_SESSION['lang'] == "en") { echo $EN_arr[8];} elseif ($_SESSION['lang'] == "az") { echo $AZ_arr[8];}?>
	                </button>
	            </a>
            </div>
            <div class="arrow bounce"></div>
        </section>
        <section class="hose">
	        <div class="text">
	        	<h1>Hose</h1>
	        	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nemo fuga laboriosam obcaecati voluptatum vero dolore molestias at commodi nulla repudiandae atque harum corporis, error delectus aut, maxime id distinctio!</p>
	        	<div class="about-footer">
	        		<div class="social">
	        			<div class="footer-grid__item footer-grid__item_social">
	        			    <ul class="social-icons">
	        			        <li class="social-icons__item">
	        			            <a href="https://twitter.com/habr_com" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_twitter" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M17.414 8.642c-.398.177-.826.296-1.276.35.459-.275.811-.71.977-1.229-.43.254-.905.439-1.41.539-.405-.432-.982-.702-1.621-.702-1.227 0-2.222.994-2.222 2.222 0 .174.019.344.058.506-1.846-.093-3.484-.978-4.579-2.322-.191.328-.301.71-.301 1.117 0 .77.392 1.45.988 1.849-.363-.011-.706-.111-1.006-.278v.028c0 1.077.766 1.974 1.782 2.178-.187.051-.383.078-.586.078-.143 0-.282-.014-.418-.04.282.882 1.103 1.525 2.075 1.542-.76.596-1.718.951-2.759.951-.179 0-.356-.01-.53-.031.983.63 2.15.998 3.406.998 4.086 0 6.321-3.386 6.321-6.321l-.006-.287c.433-.314.81-.705 1.107-1.15z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://www.facebook.com/habrahabr.ru" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_facebook" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M14.889 8.608h-1.65c-.195 0-.413.257-.413.6v1.192h2.063v1.698h-2.063v5.102h-1.948v-5.102h-1.766v-1.698h1.766v-1c0-1.434.995-2.6 2.361-2.6h1.65v1.808z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://vk.com/habr" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_vkontakte" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M16.066 11.93s1.62-2.286 1.782-3.037c.054-.268-.064-.418-.343-.418h-1.406c-.322 0-.44.139-.537.343 0 0-.76 1.619-1.685 2.64-.297.33-.448.429-.612.429-.132 0-.193-.11-.193-.408v-2.607c0-.365-.043-.472-.343-.472h-2.254c-.172 0-.279.1-.279.236 0 .343.526.421.526 1.352v1.921c0 .386-.022.537-.204.537-.483 0-1.631-1.663-2.274-3.552-.129-.386-.268-.494-.633-.494h-1.406c-.204 0-.354.139-.354.343 0 .375.44 2.114 2.167 4.442 1.159 1.566 2.683 2.414 4.056 2.414.838 0 1.041-.139 1.041-.494v-1.202c0-.301.118-.429.29-.429.193 0 .534.062 1.33.848.945.901 1.01 1.276 1.525 1.276h1.578c.161 0 .311-.075.311-.343 0-.354-.462-.987-1.17-1.738-.29-.386-.762-.805-.912-.998-.215-.226-.151-.354-.001-.59z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://telegram.me/habr_com" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_telegram" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M17.17 7.621l-10.498 3.699c-.169.059-.206.205-.006.286l2.257.904 1.338.536 6.531-4.796s.189.057.125.126l-4.68 5.062-.27.299.356.192 2.962 1.594c.173.093.397.016.447-.199.058-.254 1.691-7.29 1.728-7.447.047-.204-.087-.328-.291-.256zm-6.922 8.637c0 .147.082.188.197.084l1.694-1.522-1.891-.978v2.416z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://www.youtube.com/channel/UCd_sTwKqVrweTt4oAKY5y4w" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_youtube">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="32" height="32" viewBox="0 0 32 32">
	        			                    <path d="M3.2 0h25.6c1.767 0 3.2 1.433 3.2 3.2v25.6c0 1.767-1.433 3.2-3.2 3.2h-25.6c-1.767 0-3.2-1.433-3.2-3.2v-25.6c0-1.767 1.433-3.2 3.2-3.2zm18.133 16l-10.667-5.333v10.667l10.667-5.333z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			    </ul>
	        			</div>
	        		</div>
	        	</div>
        	</div>
        </section>
        <section class="samir">
	        <div class="text">
	        	<h1>Samir</h1>
	        	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nemo fuga laboriosam obcaecati voluptatum vero dolore molestias at commodi nulla repudiandae atque harum corporis, error delectus aut, maxime id distinctio!</p>
	        	<div class="about-footer">
	        		<div class="social">
	        			<div class="footer-grid__item footer-grid__item_social">
	        			    <ul class="social-icons">
	        			        <li class="social-icons__item">
	        			            <a href="https://twitter.com/habr_com" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_twitter" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M17.414 8.642c-.398.177-.826.296-1.276.35.459-.275.811-.71.977-1.229-.43.254-.905.439-1.41.539-.405-.432-.982-.702-1.621-.702-1.227 0-2.222.994-2.222 2.222 0 .174.019.344.058.506-1.846-.093-3.484-.978-4.579-2.322-.191.328-.301.71-.301 1.117 0 .77.392 1.45.988 1.849-.363-.011-.706-.111-1.006-.278v.028c0 1.077.766 1.974 1.782 2.178-.187.051-.383.078-.586.078-.143 0-.282-.014-.418-.04.282.882 1.103 1.525 2.075 1.542-.76.596-1.718.951-2.759.951-.179 0-.356-.01-.53-.031.983.63 2.15.998 3.406.998 4.086 0 6.321-3.386 6.321-6.321l-.006-.287c.433-.314.81-.705 1.107-1.15z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://www.facebook.com/habrahabr.ru" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_facebook" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M14.889 8.608h-1.65c-.195 0-.413.257-.413.6v1.192h2.063v1.698h-2.063v5.102h-1.948v-5.102h-1.766v-1.698h1.766v-1c0-1.434.995-2.6 2.361-2.6h1.65v1.808z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://vk.com/habr" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_vkontakte" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M16.066 11.93s1.62-2.286 1.782-3.037c.054-.268-.064-.418-.343-.418h-1.406c-.322 0-.44.139-.537.343 0 0-.76 1.619-1.685 2.64-.297.33-.448.429-.612.429-.132 0-.193-.11-.193-.408v-2.607c0-.365-.043-.472-.343-.472h-2.254c-.172 0-.279.1-.279.236 0 .343.526.421.526 1.352v1.921c0 .386-.022.537-.204.537-.483 0-1.631-1.663-2.274-3.552-.129-.386-.268-.494-.633-.494h-1.406c-.204 0-.354.139-.354.343 0 .375.44 2.114 2.167 4.442 1.159 1.566 2.683 2.414 4.056 2.414.838 0 1.041-.139 1.041-.494v-1.202c0-.301.118-.429.29-.429.193 0 .534.062 1.33.848.945.901 1.01 1.276 1.525 1.276h1.578c.161 0 .311-.075.311-.343 0-.354-.462-.987-1.17-1.738-.29-.386-.762-.805-.912-.998-.215-.226-.151-.354-.001-.59z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://telegram.me/habr_com" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_telegram" target="_blank">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24">
	        			                    <path d="M17.17 7.621l-10.498 3.699c-.169.059-.206.205-.006.286l2.257.904 1.338.536 6.531-4.796s.189.057.125.126l-4.68 5.062-.27.299.356.192 2.962 1.594c.173.093.397.016.447-.199.058-.254 1.691-7.29 1.728-7.447.047-.204-.087-.328-.291-.256zm-6.922 8.637c0 .147.082.188.197.084l1.694-1.522-1.891-.978v2.416z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			        <li class="social-icons__item">
	        			            <a href="https://www.youtube.com/channel/UCd_sTwKqVrweTt4oAKY5y4w" class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_youtube">
	        			                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="32" height="32" viewBox="0 0 32 32">
	        			                    <path d="M3.2 0h25.6c1.767 0 3.2 1.433 3.2 3.2v25.6c0 1.767-1.433 3.2-3.2 3.2h-25.6c-1.767 0-3.2-1.433-3.2-3.2v-25.6c0-1.767 1.433-3.2 3.2-3.2zm18.133 16l-10.667-5.333v10.667l10.667-5.333z"></path>
	        			                </svg>
	        			            </a>
	        			        </li>
	        			    </ul>
	        			</div>
	        		</div>
	        	</div>
        	</div>
        </section>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="../js/nprogress.js"></script>
<script src="../js/load.js"></script>
<script>
(function() {
    var body = document.getElementById("work-place"),
        startX = 20,
        startY = 20,
        w = document.documentElement.offsetWidth,
        h = document.documentElement.offsetHeight;

    body.addEventListener('mousemove', function(evt) {
        var posX = Math.round(evt.clientX / w * startX)
        var posY = Math.round(evt.clientY / h * startY)
        body.style.backgroundPosition = posX + 'px ' + posY + 'px'
    })
})()
</script>

</html>