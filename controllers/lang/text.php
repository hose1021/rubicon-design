<?php 
	//Includes
	include_once $_SERVER['DOCUMENT_ROOT'] . '/config/db.php';
	include_once $_SERVER['DOCUMENT_ROOT'] . '/models/lang.php';

	//Database connect
	$database = new Database();
	$db = $database->connect();

	$post = new Text($db);
	$result = $post->read();

	$num = $result->rowCount();

	if ($num>0) {
		$ID_arr = array();
		$RU_arr = array();
		$EN_arr = array();
		$AZ_arr = array();

		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			extract($row);

		    $text_item = array(
		    	'id' => $ID,
		    	'ru' => $RU,
		    	'en' => $EN,
		    	'az' => $AZ
		    );
		    array_push($ID_arr, $text_item['id']);
		    array_push($RU_arr, $text_item['ru']);
		    array_push($EN_arr, $text_item['en']);
		    array_push($AZ_arr, $text_item['az']);
		}

	} else {
		echo 'no items';
	}
 ?>