<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="author" content="Marx JMoura">
    <meta name="description" content="Admin 4B. Open source and free admin template built on top of Bootstrap 4. Quickly customize with our Sass variables and mixins.">
    <title>Admin 4B · Get started</title>
    <link rel="icon" href="./favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link href="../dist/admin4b.min.css" rel="stylesheet">
</head>

<body>
    <div class="app">
        <div class="app-body">
            <div class="app-sidebar sidebar-slide-left">
                <div class="text-right">
                    <button type="button" class="btn btn-sidebar" data-dismiss="sidebar"><span class="x"></span></button>
                </div>
                <div class="sidebar-header"><img src="../assets/img/Hose.jpg" class="user-photo">
                    <p class="username">Гусейнов Микаил
                        <br><small>Администратор</small></p>
                </div>
                <ul id="sidebar-nav" class="sidebar-nav">
                    <li class="sidebar-nav-btn"><a href="./../../index.php" class="btn btn-block btn-outline-light">Перейти на сайт</a></li>
                    <li><a href="content/lang.php" class="sidebar-nav-link"><i class="icon-pencil"></i> Язык</a>
                    </li>
                </ul>
                <div class="sidebar-footer"><a href="./pages/content/settings.html" data-toggle="tooltip" title="Настройки"><i class="fa fa-cog"></i> </a><a href="./pages/content/signin.html" data-toggle="tooltip" title="Выйти"><i class="fa fa-power-off"></i></a></div>
            </div>
            <div class="app-content">
                <nav class="navbar navbar-expand navbar-light bg-white">
                    <button type="button" class="btn btn-sidebar" data-toggle="sidebar"><i class="fa fa-bars"></i></button>
                    <div class="navbar-brand">Rubicon &middot; © 2018 by Hose1021</div>
                </nav>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Главная</li>
                    </ol>
                </nav>
                <div class="container-fluid">
                    <h1>Инструкция</h1>
                    <p>Ма хорошо, но дома лучше.</p>
                    <h2>Быстрое начало</h2>
                    <p>Хочешь стать геем, отсоси. Например: <a href="https://jquery.com/">тут</a> и <a href="https://popper.js.org/">тут</a>.</p>
                    <div class="callout callout-warning">
                        <h5>Rubicon</h5>
                        <p>Эта страница посвещена <strong>геям</strong>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script src="../dist/admin4b.min.js"></script>
    <script src="../assets/js/admin4b.docs.js"></script>
</body>

</html>