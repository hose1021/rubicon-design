<?php
require_once '../../../config/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . 'controllers/lang/text.php';
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="author" content="Marx JMoura">
    <meta name="description" content="Blank page is a startup point for creating your content.">
    <title>Admin 4B · Blank page</title>
    <link rel="icon" href="../../favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link href="../../dist/admin4b.min.css" rel="stylesheet">
</head>

<body>
    <div class="app">
        <div class="app-body">
            <div class="app-sidebar sidebar-slide-left">
                <div class="text-right">
                    <button type="button" class="btn btn-sidebar" data-dismiss="sidebar"><span class="x"></span></button>
                </div>
                <div class="sidebar-header"><img src="../../assets/img/Hose.jpg" class="user-photo">
                    <p class="username">Гусейнов Микаил
                        <br><small>Администратор</small></p>
                </div>
                <ul id="sidebar-nav" class="sidebar-nav">
                    <li class="sidebar-nav-btn"><a <?php echo 'href="' . $_SERVER['DOCUMENT_ROOT'] . 'index.php"'; ?> class="btn btn-block btn-outline-light">Перейти на сайт</a></li>
                    <li><a href="content/lang.php" class="sidebar-nav-link"><i class="icon-pencil"></i> Язык</a>
                    </li>
                </ul>
                <div class="sidebar-footer"><a href="./pages/content/settings.html" data-toggle="tooltip" title="Настройки"><i class="fa fa-cog"></i> </a><a href="./pages/content/signin.html" data-toggle="tooltip" title="Выйти"><i class="fa fa-power-off"></i></a></div>
            </div>
            <div class="app-content">
                <nav class="navbar navbar-expand navbar-light bg-white">
                    <button type="button" class="btn btn-sidebar" data-toggle="sidebar"><i class="fa fa-bars"></i></button>
                    <div class="navbar-brand">Rubicon &middot; © 2018 by Hose1021</div>
                </nav>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php" style="color: #212529;">Главная</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Язык</li>
                    </ol>
                </nav>
                <div class="container-fluid">
				<h1>Таблица Языков</h1>
				<table class="table table-hover">
				  <thead class="thead-light">
				    <tr>
				      <th scope="col" style="width: 3%; text-align: center;">ID</th>
				      <th scope="col" style="width: 20%";>RU</th>
				      <th scope="col" style="width: 20%">EN</th>
				      <th scope="col" style="width: 20%">AZ</th>
				    </tr>
				  </thead>
				  <tbody>
					<?php 
						for ($i=0; $i < $num;$i++) { 
							echo "<tr>";
							echo "<th scope='row' style='text-align: center;'>" . $ID_arr[$i] . "</th>
							<td>" . $RU_arr[$i] . "</td>
							<td>" . $EN_arr[$i] . "</td>
							<td>" . $AZ_arr[$i] . "</td>";
							echo "</tr>";
						}
					?>
					</tbody>
				</table>
             	</div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script src="../../dist/admin4b.min.js"></script>
    <script src="../../assets/js/admin4b.docs.js"></script>
</body>

</html>