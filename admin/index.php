<?php
require_once './config/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <title>Login | Rubicon</title>
  <link rel="icon" href="../../favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
  <link href="./dist/admin4b.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="page-sign">
      <form class="form-sign">
          <h1 class="display-4 text-center mb-5">RUBICON<!-- <img src="../images/logo.png" alt=""> --></h1>
          <div class="form-group">
              <div class="label-floating">
                  <input id="username" type="text" class="form-control" placeholder="E-mail" maxlength="50" autofocus>
                  <label for="username">Логин</label>
              </div>
          </div>
          <div class="form-group">
              <div class="label-floating">
                  <input id="password" type="password" class="form-control" placeholder="Password" maxlength="20">
                  <label for="password">Пароль</label>
              </div>
          </div>
          <input type="checkbox" id="cbx" class="inp-cbx" style="display: none">
          <label for="cbx" class="cbx">
            <span>
              <svg width="12px" height="10px" viewBox="0 0 12 10">
                <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
              </svg>
            </span>
            <span>Запомнить</span>
          </label>
          <a href="./pages/index.php" class="btn btn-lg btn-primary btn-block">Войти</a>
      </form>
  </div>
  <div style="position: absolute; bottom: 0; width: 100%;">
    <h6 style="text-align: center;">©2018 Rubicon creative Agency | All rights reserved.</h6>
  </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script src="../../dist/admin4b.min.js"></script>
<script src="../../assets/js/admin4b.docs.js"></script>
</body>
</html>