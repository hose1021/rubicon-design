-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.31-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных rubicon
CREATE DATABASE IF NOT EXISTS `rubicon` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `rubicon`;

-- Дамп структуры для таблица rubicon.langueage
CREATE TABLE IF NOT EXISTS `langueage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RU` varchar(200) NOT NULL,
  `EN` varchar(200) NOT NULL,
  `AZ` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы rubicon.langueage: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `langueage` DISABLE KEYS */;
INSERT INTO `langueage` (`ID`, `RU`, `EN`, `AZ`) VALUES
	(0, 'Услуги', 'Services', 'Servisler'),
	(1, 'Портфолио', 'Portfolio', 'İşlərimiz'),
	(2, 'Cвязь', 'Contacts', 'Əlaqə'),
	(3, 'О нас', 'About us', 'Haqqımızda'),
	(4, 'Время летит — это плохая новость. Хорошая новость — вы пилот своего времени.', 'The bad news is time flies. The good news is you’re the pilot.', 'Saat uçur - bu pis xəbərdir. Yaxşı xəbər sizin vaxtınızın pilotu.'),
	(5, 'Майкл Альтшулер', 'Michael Altshuler', 'Mikail Atşuler'),
	(6, 'Добро пожаловать!', 'Welcome!', 'Xoş gəlmisiniz!'),
	(7, 'Мы умеем', 'We may', 'Biz'),
	(8, 'Заказать', 'Order', 'SIFARIŞ'),
	(9, 'ХАХАХААХ', 'asd', 'sad');
/*!40000 ALTER TABLE `langueage` ENABLE KEYS */;

-- Дамп структуры для таблица rubicon.users
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` char(50) NOT NULL DEFAULT '0',
  `PASS` varchar(50) NOT NULL DEFAULT '0',
  `DELETED` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы rubicon.users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
